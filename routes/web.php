<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/reviews/{id}', 'ReviewsController@showReviews')->name('showReviews');

Route::get('/find-restaurant/{restaurant}/{city}', 'RestaurantController@findRestaurant')->name('findRestaurant');

Route::post('/add-review', 'ReviewsController@addReview')->name('addReview')->middleware('auth');

Route::get('logout', 'Auth\LoginController@logout');


