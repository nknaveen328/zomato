<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;





$factory->define(\App\Restaurant::class, function (Faker $faker) {
    $type = ['cafe','restaurant','bar'];
    $banner = [
        'https://cdn1-www.musicfeeds.com.au/assets/uploads/6d4b62960a6aa2b1fff43a9c1d95f7b2-640x360.jpg',
        'https://us.123rf.com/450wm/viteethumb/viteethumb1801/viteethumb180100012/92938082-bar-de-mesa-superior-con-fondo-de-restaurante-blur-bar.jpg?ver=6',
        'http://www.home-ed-press.com/wp-content/uploads/2019/02/Restaurants.jpg',
        'https://media-cdn.tripadvisor.com/media/photo-s/12/64/31/c8/restaurant-in-the-background.jpg'
    ];
    $city = ['CHENNAI','KOLKATA','DELHI','MUMBAI'];

    return [
        'name' => $faker->name.' '.$type[rand(0,2)],
        'city' => $city[rand(0,3)],
        'address' => $faker->address,
        'banner_img' => $banner[rand(0,3)]
    ];
});
