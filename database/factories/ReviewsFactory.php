<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Reviews::class, function (Faker $faker) {
    return [
            'review' => $faker->text,
            'rating' => rand(0,5),
    ];
});
