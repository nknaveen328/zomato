<?php

use Illuminate\Database\Seeder;

class RestaurantTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Restaurant::class,500)->create()->each(function ($foo){
            for($i=0;$i<6;$i++){
                \App\Reviews::create([
                    'user_id' => rand(1,100),
                    'restaurant_id' => $foo->id,
                    'review' => Faker\Factory::create()->text,
                    'rating' => rand(1,5)
                ]);
            }
        });
    }
}
