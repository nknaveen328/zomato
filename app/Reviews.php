<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{

    protected $fillable = [
        'review','rating'
    ];

    protected  $table = 'reviews';


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function restaurant(){
        return $this->belongsTo(Restaurant::class);
    }


    public function withRestaurant($restaurant){
        $this->restaurant()->associate($restaurant);
        return $this;
    }

    public function withUser($user){
        $this->user()->associate($user);
        return $this;
    }
}
