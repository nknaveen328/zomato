<?php

namespace App\Http\Controllers;

use App\Restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    /**
     * @param $restaurant
     * @param $city
     * @return false|string
     */

    public function findRestaurant($restaurant, $city){

        $restaurants = Restaurant::where('city', $city)
            ->where('name','LIKE','%'.$restaurant.'%')
            ->distinct()
            ->limit(10)
            ->get();

        return json_encode($restaurants);

    }
}
