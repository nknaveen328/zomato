<?php

namespace App\Http\Controllers;

use App\Restaurant;
use App\Reviews;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $featured = Reviews::where('rating','>','4')->with('restaurant')->get()->take(4);
        foreach ($featured as $review){
            $review->avg_rating = $this->getAverageRating($review);
        }

        $popular = Reviews::orderBy('rating','desc')->with('restaurant')->first();
        $popular->avg_rating = $this->getAverageRating($popular);
        $popular->review_count = $popular->restaurant->reviews->count();


        return view('main')->with(compact('featured', 'popular'));
    }


    private function getAverageRating(Reviews $review){

        $reviews = $review->restaurant->reviews;
        if(count($reviews) > 0){
            return round($reviews->sum('rating')/count($reviews));
        }else{
            return 0;
        }

    }
}
