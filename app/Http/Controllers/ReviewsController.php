<?php

namespace App\Http\Controllers;

use App\Restaurant;
use App\Reviews;
use App\User;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    public function showReviews($id){
        $restaurant = Restaurant::find($id);
        $reviews = $restaurant->reviews;

        return view('restaurant')->with(compact('restaurant', 'reviews'));
    }

    public function addReview(Request $request){

        $request->validate([
            "restaurant" => "required",
            "review" => "required",
            "rating" => "required"
        ]);


        $review = new Reviews($request->all());
        $save = $review
            ->withUser(User::find(auth()->id()))
            ->withRestaurant(Restaurant::find($request['restaurant']))
            ->save();


        if($save){
            return redirect('reviews/'.$request['restaurant'])->with('status', 'Successfully Posted the review !');
        }else{
            return redirect()->back()->withErrors(['Reviews cannot be saved right now. Please try again later.']);

        }
    }
}
