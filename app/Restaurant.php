<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{

    protected $fillable = [
        'name','city','address','banner_img'
    ];

    public function reviews(){
        return $this->hasMany(Reviews::class, 'restaurant_id');
    }
}
