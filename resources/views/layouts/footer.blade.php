<section id="footer">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-3 col-xs-12 col-sm-3">
                    <p class="sub-heading rf bold">CLASSIFIEDS</p>

                    <ul class="footer-list">
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Licensing</a></li>
                        <li><a href="#">Press and Media</a></li>
                        <li><a href="#">Trademark</a></li>
                        <li><a href="#">Contact</a></li>

                    </ul>
                </div>

                <div class="col-md-3 col-xs-12 col-sm-3">
                    <p class="sub-heading rf bold">RESTAURANTS</p>
                    <ul class="footer-list">
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Licensing</a></li>
                        <li><a href="#">Press and Media</a></li>
                        <li><a href="#">Trademark</a></li>
                        <li><a href="#">Contact</a></li>

                    </ul>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-3">
                    <p class="sub-heading rf bold">ABOUT</p>
                    <ul class="footer-list">
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Licensing</a></li>
                        <li><a href="#">Press and Media</a></li>
                        <li><a href="#">Trademark</a></li>
                        <li><a href="#">Contact</a></li>

                    </ul>
                </div>

                <div class="col-md-3 col-xs-12 col-sm-3">
                    <p class="sub-heading rf bold">ADVERTISE</p>
                    <ul class="footer-list">
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Licensing</a></li>
                        <li><a href="#">Press and Media</a></li>
                        <li><a href="#">Trademark</a></li>
                        <li><a href="#">Contact</a></li>

                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>