<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <script src="https://kit.fontawesome.com/00ffa4a415.js" crossorigin="anonymous"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/main.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
</head>

<body>

<section id="main">
    @include('layouts.navbar')

    <!--    <div class="inner"></div>-->
    <div class="overlay"></div>


    <div class="content">
        <i class="fas fa-cloud-meatball" id="page-logo"></i>
        <h1 class="fr">Find the Best Restaurants, cafes and bars !</h1>
        <form action="/search-restaurant" method="post" name="search-restaurant">
            <select name="f-city" id="f-city">
                <option value="CHENNAI">CHENNAI</option>
                <option value="DELHI">DELHI</option>
                <option value="KOLKATA">KOLKATA</option>
                <option value="MUMBAI">MUMBAI</option>
            </select>
            <div class="input-group">
        <span class="input-group-addon">
          <div class="dropdown fr">
            <a href="#" class="dropdown-toggle" id="city" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
<!--                <i class="material-icons">location_searching</i>-->
              <span>SELECT</span><i class="material-icons">arrow_drop_down</i>
            </a>
            <ul class="dropdown-menu">
              <li><a href="#" class="city">CHENNAI</a></li>
              <li><a href="#" class="city">DELHI</a></li>
              <li><a href="#" class="city">KOLKATA</a></li>
              <li><a href="#" class="city">MUMBAI</a></li>
            </ul>
          </div>
        </span>
                <input type="text" name="f-name" id="f-name" class="form-control" placeholder="&#xF002; &nbsp;&nbsp; Search for a restaurant, cafe's or bars" style="font-family:Arial, FontAwesome">
                <span class="input-group-btn">
        <button class="btn btn-primary" id="search" type="submit">SEARCH</button>
        </span>
            </div>

        </form>
    </div>
</section>


<section id="feature">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <p class="heading fr">FEATURED</p>

                @foreach($featured as $reviews)
                    <?php //dd($reviews->restaurant); ?>
                    <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12">
                    <div class="card">
                        <div class="card-image">
                            <img src="{{$reviews->restaurant->banner_img}}" alt="">
                        </div>
                        <div class="card-content text-center">
                            <div class="resto-info">
                                <p class="sub-heading fr bold"><a href="/reviews/{{$reviews->restaurant->id}}">{{$reviews->restaurant->name}}<a/></p>
                                <p class="resto-address">{{$reviews->restaurant->address}}</p>
                            </div>
                            <div class="about-resto">
                                <i class="fas fa-ice-cream"></i>
                                <p class="rating">
                                    <?php

                                    for($i=1;$i<=5;$i++){
                                        if($reviews->avg_rating >= $i){
                                            echo '<span class="fa fa-star checked"></span>';
                                        }else{
                                            echo '<span class="fa fa-star"></span>';
                                        }
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
</section>

<section id="popular">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <p class="heading fr">POPULAR </p>
                <div class="col md-3 col-sm-4 col-xs-12">
                    <img id="popular-image" src="https://media-cdn.tripadvisor.com/media/photo-s/12/64/31/c8/restaurant-in-the-background.jpg" alt="">
                </div>
                <div class="col md-9 col-sm-8 col-xs-12">
                    <h3 class="fr bold"><a href="/reviews/{{$popular->restaurant->id}}">{{$popular->restaurant->name}}</a></h3>
                    <p class="fr bold gray">
                        <span><i class="fas fa-map-marker-alt"></i></span>
                        <span>{{$popular->restaurant->address}}</span>
                    </p>
                    <p id="popular-rating">

                        <?php
                            for($i=1;$i<=5;$i++){
                                if($popular->avg_rating >= $i){
                                    echo '<span class="fa fa-star checked"></span>';
                                }else{
                                    echo '<span class="fa fa-star"></span>';
                                }
                            }
                        ?>

                        <small>{{$popular->review_count}} Reviews</small>

                        <small><span><i class="fas fa-ice-cream"></i></span>Restaurant</small>

                    </p>
                    <p class="fr gray">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident
                    </p>
                </div>
            </div>
        </div>
</section>


@include('layouts.footer')



<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha256-nuL8/2cJ5NDSSwnKD8VqreErSWHtnEP9E7AySL+1ev4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
<script src="{{url('js/smart_search.js')}}"></script>

<script>

</script>
</body>

</html>
