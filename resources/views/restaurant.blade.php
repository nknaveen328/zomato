<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>{{$restaurant->name}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
    <script src="https://kit.fontawesome.com/00ffa4a415.js" crossorigin="anonymous"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/main.css')}}">
    <link rel="stylesheet" href="{{url('css/res.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

</head>

<body>

@include('layouts.navbar')

<section id="review">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <h1 class="title fr">REVIEWS</h1>
                <div class="col md-3 col-sm-4 col-xs-12">
                    <img id="popular-image" src="https://media-cdn.tripadvisor.com/media/photo-s/12/64/31/c8/restaurant-in-the-background.jpg" alt="">
                </div>
                <div class="col md-9 col-sm-8 col-xs-12">
                    <h3 class="fr bold">{{$restaurant->name}}</h3>
                    <p class="fr bold gray">
                        <span><i class="fas fa-map-marker-alt"></i></span>
                        <span>{{$restaurant->address}}</span>
                    </p>
                    <p id="popular-rating">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>

                        <small>{{$reviews->count()}} Reviews</small>

                        <small><span><i class="fas fa-ice-cream"></i></span>Restaurant</small>

                    </p>
                    <p class="fr gray">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident
                    </p>
                </div>
            </div>
        </div>
</section>

<section id="review-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <p class="sub-heading bold">USER REVIEWS</p>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul id="error-list">
                            @foreach ($errors->all() as $error)
                                <li><p class="fr">{{ $error }}</p></li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('status'))
                    <div class="alert alert-success">
                        <p class="fr">{{ session('status') }}</p>
                    </div>
                @endif

                @foreach($reviews as $review)
                    <div class="col-md-6 col-sm-6">
                        <div class="review-holder">
                            <div class="review-header">
                                <div>
                                    <span><img class="ur-img" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEhUQEhIVFRUWFRUWFRUVFRUVFRUQFRUWFxUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0fHSUtKy0rLSsrLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIARMAtwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAACAQMEBQYABwj/xABAEAACAQIDBgMFBQYDCQAAAAABAgADEQQFIQYSMUFRYXGBkRMiMqGxI1JywdEHFEKC4fAkYpIWNENjorLCw9L/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQIDBP/EACIRAQEAAgICAgMBAQAAAAAAAAABAhEDIRIxQVETMnFhIv/aAAwDAQACEQMRAD8Av1WOKsVVjiiaZIqxwLFAhgQOCzrQwIoEAAIto5adaA3uzisctEtAaIgkR4iCRAYKwCsfIglYEcrG2WSCsBlgRWWMusllY0ywIbrGXSTWWMusqILpI705PdIzUWBXuk6SHSdCtaohqJyiOASDgIYEQCGBAUCFaIBCAgJaLaKBFtAG0S0O060BsiCRHbQSIDREEiOkQCIDJEBhHiIDCAwyxtlj5EBhAissaZZKZY0yyiI6xh1kxljLrCITpFjzrOgaUCGBEUQwJFKIYiAQgICgQhOAi2gdadaLacYCWjGMxVOkpqVGCqOJJtM5tbteuGG5SG/UOl/4E7nqe0wS4jF4x95qjVCNd0gEAdl4CZyy01jjt6HR2so1GsgO795vdv4XiHbDDBtxw6HqRceomdyujTf3D7jgcLWB8ucmphUqH2WIpi/8DAmzfh5g9py/JlHX8eLS4fNqL298DeNlubXHnJxtMhXykBCoIddCN7jfuevK8zy7Q1sOSgYi38JOq/ytoR4azWPLv2zeP6emkQCJj8k27pu3s65VSfhqAWVuxFzuma+nVVhdSDOsu3MDCAyx4xtoQwyxthJDCNMIEd1jLiSWWNsJREZZ0eZZ0C+EMCIBCEgIQgIghCAsKIIsAXewmB2s2od3/dcOd03s1QdfuqevfrpLnbbOjh6VlPvsCF1tbqR3nkVJ3DBmvYn4uIvzvaYyy+G8cWzoZciqtT4gR7wNyNeoHQ3178JU5dVXD1yu8LEncYkC68QL8L8rHj2h4bNWVgWYAEixuSm9101F+frLPF5bQrghwqN3AGvXeBAYTn/XT+JObUzWUPTJ311G7ZWBEz+I2krod113XHUWWoBwJHJu4kipkOKoC9KqzINd0EMB4XuPnKDN81rN7tQXtp7wBOn+bWTQu6m2O+L6q3Ucm79j6SNjcfRxiWayV14EaB1HLjx/vvMgxHEC30ibx76TXhE8qlHDVLndF+tvzEt8i2nxWHYKDdeAVtQPD9JQ+2J4k/W8KgbsPGbm2K9v2ez0YlLlCrDjzU9wQZbmY3Y5t1gLi1uRvqevebQzcYNERthHmEbMBhhG2EfYRthKGHE6EwnQLsQhBEMSKIQhBEIQhRFM4RvFE7jW47p+kK8S/aDmLVsU2p3UFhr8hKfLceaR95zbmLbw8Lc52bm9Rmve5v58LeU7Jcqeu2gnK6126z30sHxWHqaexY3/AIk931U3lnl6MfdT2g/FrYdAOE1OUbCqAGbU/Ka3BbPIulpxuVvp2mMntgsNlmJf3QRbndDp4e9E/wBhnY3Ziet56zh8sC8BCbCdo1V3Pp5BR2FuGB5HTwkTHbGlVuBPYDhQL6SvxWFvpaZ3Z8tdX4eK43Zt1XetM9TWzEMNRPds1wC7trcp5JtNghSq35GdOPPfVcuTCa3Fps5m37uysD7p0ZTrp2M9VwtYOoccCLieA0MRbTl9J7XshU3sJSN7+7O+Lz5LYiNsI40AzTJphGmjzRtpQywnRWiQLkQxAEJZAYhCCIQgEJA2grtTw1V1+IIxHpJ4jeKphkZTzUj5QPnDEsWNupnr+wGSKtJTbUi88mVL1FUc3sPW0972e3aVNQeQnDk+no4vtpMNhQABJ1OiJlcy2woUB7x/vwlRQ/aXhmbdBN/lMR0elbojb0pmMJtGji6tJFbPABe8eUTxq0rURIFamBMpm37QKVIkEFiOhlUn7SqLngZLNtemmzcAKTPHNtnBPnPSMRtFSqpYm1xpPMdq159DGE/6Tk/VmKR1tPcthb/uVLwPpc2nhrcbie87JUPZ4Wkp47gPqJ6o8lWzQDHDGzKhto20daNmA0wnRXnSi0EKCsISAxCEAQhAcESq1hOESotwR1EK8KxeDFGrXqBhv0WDU9LgksxvbpYeE1WW47E1KKVqtZzvgturZFCAkXJUX5SHgcvFLHpvDePtHBv9yzAAzX7LZDTfDHCtcnD1alIrc/Dvl6RPW9N0PnOOWXT0YYzbJYnaSmoO5TqVLcWNWra/Yb2spa2cvUuxQbt+Nt+w5Eb956jW2bdCQtCm6/6T56axv/Zwtq9Gkg+6FB16kkW+UzMluHe9sBhM6r4cqUpiqH0VRvBidNABe58BJOb7V4reFKrgmobw09oaik24kXRbgdpvdm8npDHBlVd3DIQbAWGJrWO6OhWnrb/miSf2wYNamHVjxpVFqA9FsRU8t1ifIR18wu/ivIK2aC9hRpsx6oGB8nufnItHMabGzLTU9FooPmB+U3+H2WVbMqIxGobmRy1Eg4zIFDFhhGDdV3SD53l8ppPx21l/bMfha3oR8rSCar4h/wB3YKDe29fXT/KTrNPR2fIv7hp34DQ+vSZ1sJalVr82Zirc9wGwse4F/OXGwzliqwuAJrrQJF98KTy4z3LA1AAF6ACeIZQLVUb/ADj6z1vA4q9p1jz5RpAYBgUKlxHDKhsxsxxjAMBtp05p0oshDBjYhiFGIQgCEJAYiwRCEDBZ3ljpjkrDVGJVuz7pIPn+U2OXYekW9qd5H3QC9NmQsBwDAGz25bwNpW7UtuKr6WDrfwvxjuBx6gAdp5709mMlaNuH+8VvSgf/AFyrzO+6ft6x8WRB600B9DI+LzukgILi4HDnaRsD/iPtWPuL72795RqZm5fTc457rR7NYGnSootMWUXJJvd3Y3ZyTqSTrcxva5SylhqE94i17gDUWnZZtHh6oLI6nd0IuP7tI2dZ5SRCxYWkt6STtldlyoXcpVnCAncHuuoW992zDeAF7AA2taXrUap4V6XnQJPyqiZ/LXSpvYigAq+6Gpjk9rtw8jLajjkIklW4/RnMcud1KvX0IsRSprTJU8RvEsRccxYzAbWFEpsiABQAqgcAALAD0myzfMQFNjPNs+qsxVeJJLHw4Cbw7rnyTUV+Wj3l7EGbbL8b3mKprufQd+stsBirWnfF5s/p6XlmIuJZgzJZHipqKVS4m2BmNmGYEgBp05p0osAYYjawwYBiEIAMISKcEWADCECBneCFamUOnQzA4bFPvFD8SkqfxDQ/Sej4o6Ty7MKnssXU6Fg3k3H53nPkx3HTjysujFeu1WqULWRSN9j35CegZXiqXsrK1wFt00tM7RyGjWb2isQXHow52jWLyHH4c/ZlKqn+Q+drieed+nq7tYKs9XC1W9mxGpHYjuOci5hm1atYO+g5DQefWabGZJinLA4bU6/GungCRM82Wuv/AAm89J3n+ueWGXpo9h81FGnUVjbe3SOmmh/KOYzOSr79NrqeI5+IlRgsurP7qKNdNSTbWXr7LJSUF3LFjYkaAddJyy1vtqeUhjMcUdCTofzmfxVdS5YnhYAdhLHOsUr1CKeiqLDy4TOMdZ048enHPLs9VrljfgOQkrC1JAEkYc6zq43ts8jr2tNrgqtxPPMqa1psssraCaZX14JgI0UmFC06IZ0CeDDEbBhgyKcEIGNgwwYBQrwBFvAaxR0nl22a2qe0H4W8OIPr9Z6fijpPPNolBqbp4E2i+iez+y2LuQBw5eM3pZioInj2WY04erungPmL8f76T1nJcwpuo1uCJ5MpqvXjltnNpcWQDvJpzP5zGU8SXudzTp26z2XGJQdbGxBlFWwVBb7qqNOgjbflftkcqpMToN0czKnajNzewOg0A7dZq83xlKmhAI7+E80zLFCo7Py5RhN3dc+TLrSNUrEDueMYEkth7U988SR5CRhPTi8+QxJGFGsjiTMGNZplfYEcJpMuq2mfwQlzhDaVlqMPUuI/eV2EqaScGhSkxIhM6BPBhiNKYYMKdEIRsGEDIHAYsbBi3gN4nhMDtAn2q/iH1m8rnSZHOaV3X8Qj4J7YrPKBufHQwMtzitQI9427azQ5tg76zM4nCW6TzyyzVejPGy7i/O1dQjXryNoGO2kc6A8uRv8ASZpahtY3P5RqpideHP18ZfCJ50/jMZUqE3PEayJh8PdgOQN4qXJ0ljQpbq3PEy26jMm6bzAfZnsRKgS+q0CyMOo+coiCNDNYXpOSdiWTcFxkFZPwfGdHNosDwlrRlVgZa0ZWVrhHlkjymw7SwpVIEu86AGiQqyBhiMqY6DIpwQhGwYQMBwRbwAY/h6DubKL/AEgRa3CUeMwpLA20Gs3i5GEXebVrXtyEz2arOfJnqadOPDd2y+Jo3vKLFYQTVV0lZi6E88evUsZarlo4SA+WWmorUZAr0yZvbn4xU0cEOckmnc2kkUodKlJaTHQRQ0kWrlSvxGsuadOPYbDzPlpq4ys5h9lWc7oexPC40iYjIsRh9aiG1/iGonoGUYO9RfX0mrq4NWXdYAjoZ348rZ283JjJenkGDMtqJmwxmx1BtUG4e3D0lLi9na9LW2+Oo4+k7SuOkOmZMpNIA0kik8CxVokZSpOgXKtHVMjKY6pkaPgxxASbAXjuAy6pU14L1P5TU5blaU+WvU8Y2K/LMiJ96roOS/rL+jh1QWUAR2KZNqFxvDymHz7DFXI5cR4TauSDeQc3wK1kuOPI9+hmM8dxvDLVecV1kOsukusfhGUlSLGVNVSNJ59PTKqK4kCsJZ4pZW1jKGd2OUUh0qJMscFgSZKEw9GTqGFsZYYXAWEucuyjeNyLL16+ETG1LlITIMDYGoRx0HhzlqFkhlAG6OXHsOkFVnpxmpp5cru7EqQxSEVRHFlRUZjs/Rq6lbHqNDMvjtmK1O5T3h856FBZJdpp5VqDYgg9509Dx+T0avxKPEaH1iy7TTKYamzkKoJJ5CarK8iC2apqenIfrJWU5YlFbDU825n+ks1k2sg6VMDlHwY0DFBkaPAwrxm84tCHHEjOCNR5jr/WOe1gMwlEPE0KdUWYajyYTP5hs43FCD24GaOsgP68/WMkuOBB8dD6j9Jm4ytY5WemBxmSOOKkeUrTk+s9JqVzzQ+RBjDV15o3+mc7xOk5r9MTh8utoBLfA5Q/JfM6S+/eBypt6AfUxRWc8lX/AKj+QlnHEvLQ4TLFXV9bekmGtfRNB97/AORz8ZHCg/ESx78PIcI7e83JI522kNhwiLFtOAlQ4sMRsQrwHAYsANOLQFadG2edCpStD3pENS0VakCWGhB5D9tOFaBN34haR1eHeEGxjbGKTAaALNGy0JoEAXjREeMAiA0RBIjhiEShFEeAjawxIFtEimJA68RmiEwHMAvaTkfjI9RtJ1NoB1q3vW6AfOdK+tUvUcfh+kSBZUsQKiBx/Z5wmrWEqsBW3Kr0eTDfT/yH09ZJdr2gSqdQySkiUjeTFgPJDjYaLvQDJgMZxMEmAJMS84mDeAsEznqAC5IA6k2Er6+d4VfirJ5G/wBI3IJ0GUz7VYIcaw/0v+kfw2e4WpolZCel7H0Mm4aWQhAxpXB1BuO2sIGUHeITEvBLQFMbYzmaNO8BKvCNho1iKuh8Iwla8AKbXq1PEf8AbOjeBb7Wp5fSdAiZpidwrWHFDc90OjfL6S3o1gw3gdDr5TNrihVw6VOos3Te53ElbMYi9Nqd9aZt/LxX5fSBpaNSS0eUuGqaywp1IE4PDVpEV45vwJBaDvSP7W/CM4/Gikm8ePADqYLde0tmgPc87eHGQcqxRqKWJ5yYWiz7JdzcMNgKRN2XePViW+sNcNTHBFHgohFo3UrBQWJsALmAlTDUzxRT4qJDrZNhW40UP8omaxW3ANdaVNRuFrFzxP4bkDtc6a85W5ntVjbO9vYID9nvpZnN/hseJtc3Gmkz5LpraOQpSbfoO9M/d3i1M9ipkxcx3WFOrZS3wN/Ax+7fk3DQ8eXSeZJtxjR/Gh8UH5Wj+K24atTNKtRXXgycVYcDute46i+ovM5b9xXqZaAzzNbMZ/Sq01p+0u4FrG+98+I769zL1nm8buMjd4y9SA9SR6lSUdiKgIIkahV1tGcTWteQMJi7uR2v6wDx2brhxWqtwBQDuSbTpktq67PWFHl8Z7mxA/OdILTZioTh6oJuA1x4kAmTNlnIxNQX0KDTwP8AWdOlGnwx1k9J06A9TMWsZ06AdHhKfapjuL+IfQzp01j7jny/pT2zx+x/mP5SyMWdGf7VeP8ASAmb25qsuHNja5sfC4H5mdOmL6dI8tqcbRcegFRlHBWIGpNgPGdOmPhDaIDeMVP0nTpILDZtyMRSINvtKfoXAPyJ9Z6TspiHfDguxYhqgudTZajgC/gBOnTU9r8LJzIlUxZ02iqxx4ymy5z7VteQnToELFD/ABjdl/WdOnRGa//Z" alt=""></span>
                                </div>
                                <div class="review-details">
                                    <span class="fr"><span class="fr bold">{{$review->user->name}}</span><br><small>{{\Carbon\Carbon::createFromTimeStamp(strtotime($review->created_at))->diffForHumans()}}</small></span>
                                </div>
                                <div class="pull-right">

                                    <?php
                                        for($i=1;$i<=5;$i++){
                                            if($review->rating >= $i){
                                                echo '<span class="fa fa-star checked"></span>';
                                            }else{
                                                echo '<span class="fa fa-star"></span>';
                                            }
                                        }
                                    ?>
                                </div>
                            </div>

                            <div class="review-content">
                                <p class="fr">{{$review->review}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-md-12 col-sm-12">
                @if(auth()->user())
                    <p class="sub-heading bold">WRITE REVIEW</p>

                    <form action="/add-review" method="POST" id="add-review" >
                        @csrf
                        <input type="hidden" name="restaurant" value="{{$restaurant->id}}">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <p class="rating-label fr bold">Review</p>
                                <textarea name="review"  id="user-review" required></textarea>
                            </div>

                            <div class="form-group">
                                <p class="rating-label fr bold">Rating</p>
                                <div class="rating">
                                    <input type="radio" id="star5" name="rating" value="5" required/><label class = "full" for="star5" title="Awesome - 5 stars"></label>

                                    <input type="radio" id="star4" name="rating" value="4" required/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>

                                    <input type="radio" id="star3" name="rating" value="3" required/><label class = "full" for="star3" title="Meh - 3 stars"></label>

                                    <input type="radio" id="star2" name="rating" value="2" required/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>

                                    <input type="radio" id="star1" name="rating" value="1" required/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>

                                </div>
                            </div>

                            <div class="form-group" >
                                <input type="submit" class="btn btn-primary" value="submit" id="submit-review">
                            </div>
                        </div>
                    </form>
                    @else
                    <p class="sub-heading bold">Please <a href="/login"> Login</a> to write Review</p>

                @endif
            </div>
        </div>
    </div>
</section>

@include('layouts.footer')

<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha256-nuL8/2cJ5NDSSwnKD8VqreErSWHtnEP9E7AySL+1ev4=" crossorigin="anonymous"></script>

</body>

</html>
