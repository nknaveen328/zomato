$(document).ready(function() {
    var e;
    $(".city").click(function(a) {
        a.preventDefault(), 
        e = $(this).text(), $("#city").html($(this).text() + '<i class="material-icons">arrow_drop_down</i>')
    });
    var a = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace("name"),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: "/find-restaurant/%RESTAURANT/%QUERY",
            replace: function(a) {
                return a.replace("%RESTAURANT", $("#f-name").val()).replace("%QUERY", e)
            }
        }
    });
    $("#f-name").typeahead({
        hint: !1,
        highlight: !1,
        minLength: 1
    }, {
        source: a.ttAdapter(),
        name: "usersList",
        source: a,
        displayKey: "name",
        templates: {
            empty: ['<div class = "suggestion no-restaurant">No Restaurants Found!</div>'],
            header: ['<div class="list-group search-results-dropdown">'],
            suggestion: function(e) {
                return  res_data = e, '<a href = "/reviews/' + res_data.id + '" data-id="' + res_data.id + '" data-name = ' + res_data.name + ' class="list-group-item suggested-res"> <span class="suggested-name bold">' + res_data.name + '</span> <br><span class="suggested-address">' + res_data.address + "</span></a>"
            }
        }
    })
});